#include <IRremote.h> // https://github.com/Arduino-IRremote/Arduino-IRremote


#define PIN_LED_R 4 
#define PIN_LED_G 5 
#define PIN_LED_B 6  

#define PIN_DETECT 2

#define PIN_BEEPER 10

#define PIN_LED 9

#define PIN_RV 4

#define PIN_IR_INPUT A0
IRrecv irrecv(PIN_IR_INPUT);
decode_results results;

int detect = LOW;
int led=0;
int pwm = 0;
bool isHandLed = true;

void setup() {
  pinMode(PIN_LED_R, OUTPUT); 
  pinMode(PIN_LED_G, OUTPUT); 
  pinMode(PIN_LED_B, OUTPUT); 

//  pinMode(PIN_DETECT, INPUT);

  pinMode(PIN_BEEPER, OUTPUT);

//  pinMode(PIN_IR_INPUT, INPUT);

  pinMode(PIN_LED, OUTPUT);

  Serial.begin(9600);
  irrecv.enableIRIn();
}

void loop() {
  if (irrecv.decode(&results))         
  {
    Serial.println(results.value, HEX); // Печатаем полученные показания
    irrecv.resume();                    // Получение следующего значения

    if(results.value==0xE0E036C9 || results.value==0xFFA25D){
      ledOff();
      Serial.println(F("Red"));
      digitalWrite(PIN_LED_R, HIGH);
    }
    if(results.value==0xE0E028D7 || results.value==0xFF629D){
      ledOff();
      Serial.println(F("Green"));
      digitalWrite(PIN_LED_G, HIGH);
    }
    if(results.value==0xE0E06897 || results.value==0xFFE21D){
      ledOff();
      Serial.println(F("Blue"));
      digitalWrite(PIN_LED_B, HIGH);
    }
    if(results.value==0xE0E0A857 || results.value==0xFFB04F){
      ledOff();
      Serial.println(F("White"));
      digitalWrite(PIN_LED_R, HIGH);
      digitalWrite(PIN_LED_G, HIGH);
      digitalWrite(PIN_LED_B, HIGH);
    }
    if(results.value==0xE0E0B44B || results.value==0xFF9867){
      ledOff();
      Serial.println(F("Exit"));
    }
    if(results.value==0xFF10EF){
      isHandLed = !isHandLed;
      Serial.println(F("LED mode"));
    }
    
    if(results.value==0xE0E016E9 || results.value==0xFF6897){
      tone(PIN_BEEPER, 2000); 
      delay(200);
      noTone(PIN_BEEPER);
      irrecv.enableIRIn();
    }
  }
  
//  detect = digitalRead (PIN_DETECT);
//  if(detect==HIGH){
//    analogWrite(PIN_LED_R, 255);
//  }else{
//    analogWrite(PIN_LED_R, 0);
//  }

  
  if(isHandLed){
    pwm = analogRead(PIN_RV);
    pwm = map(pwm, 0, 1023, 0, 255);
    pwm = constrain(pwm, 0, 255);
    analogWrite(PIN_LED, pwm);
    delay(100);
  }else{
    for(int i=0;i<=150;i++) {
      analogWrite(PIN_LED, i);
      delay(5);
    }
  }
}

void ledOff(){
  digitalWrite(PIN_LED_R, LOW);
  digitalWrite(PIN_LED_G, LOW);
  digitalWrite(PIN_LED_B, LOW);
}

void loopRGB()
{
 //   digitalWrite(PIN_LED_R, HIGH); // включаем красный свет
//   digitalWrite(PIN_LED_G, LOW);
//   digitalWrite(PIN_LED_B, LOW);
// 
//   delay(1000); // устанавливаем паузу для эффекта
// 
//   digitalWrite(PIN_LED_R, LOW);
//   digitalWrite(PIN_LED_G, HIGH); // включаем зеленый свет
//   digitalWrite(PIN_LED_B, LOW);
// 
//   delay(1000); // устанавливаем паузу для эффекта
// 
//   digitalWrite(PIN_LED_R, LOW);
//   digitalWrite(PIN_LED_G, LOW);
//   digitalWrite(PIN_LED_B, HIGH); // включаем синий свет
// 
//  delay(1000); // устанавливаем паузу для эффекта
  
   // плавное включение/выключение красного цвета
   for (int i = 0; i <= 255; i++) {
      analogWrite(PIN_LED_R, i);
      delay(2);
   }
   for (int i = 255; i >= 0; i--) {
      analogWrite(PIN_LED_R, i);
      delay(2);
   }

   // плавное включение/выключение зеленого цвета
   for (int i = 0; i <= 255; i++) {
      analogWrite(PIN_LED_G, i);
      delay(2);
   }
   for (int i = 255; i >= 0; i--) {
      analogWrite(PIN_LED_G, i);
      delay(2);
   }

   // плавное включение/выключение синего цвета
   for (int i = 0; i <= 255; i++) {
      analogWrite(PIN_LED_B, i);
      delay(2);
   }
   for (int i = 255; i >= 0; i--) {
      analogWrite(PIN_LED_B, i);
      delay(2);
   } 
}
